var stickyCrumb = {

    settings: {
        /*
        config.containerSelector: '.sc-container',//breadcrumb container that will be detached from page and "sticked" to top or where ever
        listSelector: '.sc-container > ul',//navigation item container,
        breadcrumb: 1,//enabled 0/1
        breakpoint: 992,//this will be activated when screenwidth is below this number
        touchOnly: true,//if activated only on touch enabled devices
        toTopButton: 1,//show 'Go to top' button 0/1
        toTopIcon: null// string svg image
        */
    },
    config: {
        //defaults
        containerSelector: '.sc-container',
        listSelector: ' > ul',
        breakpoint: 992,
        touchOnly: 1,
        breadcrumb: 1,//enabled 0/1
        totop: {
            size: 50,//element size in px
            button: 1,//enabled 0/1
            icon: '<svg aria-hidden="false" focusable="true" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="#ffffff" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"></path></svg>'
        }
    },

    containerElement: {},
    listElement: {},
    stickyElement: null,
    isTouchSupported: ('ontouchstart' in window),

    start: function(){

            if(Object.keys(stickyCrumb.settings).length > 0){

                    if(typeof stickyCrumb.settings.containerSelector !== 'undefined') {
                        stickyCrumb.containerElement = document.querySelector(stickyCrumb.settings.containerSelector);
                        stickyCrumb.config.containerSelector = stickyCrumb.settings.containerSelector;
                    }
                    if(typeof stickyCrumb.settings.listSelector !== 'undefined') {
                        stickyCrumb.listElement = document.querySelector(stickyCrumb.config.containerSelector + ' ' + stickyCrumb.settings.listSelector);
                        stickyCrumb.config.listSelector = stickyCrumb.settings.listSelector;
                    }
                    if(typeof stickyCrumb.settings.breadcrumb === 'number') stickyCrumb.config.breadcrumb = stickyCrumb.settings.breadcrumb;
                    if(typeof stickyCrumb.settings.breakpoint === 'number') stickyCrumb.config.breakpoint = stickyCrumb.settings.breakpoint;
                    if(typeof stickyCrumb.settings.touchOnly === 'number') stickyCrumb.config.touchOnly = stickyCrumb.settings.touchOnly;
                    if(typeof stickyCrumb.settings.toTopButton === 'number') stickyCrumb.config.totop.button = stickyCrumb.settings.toTopButton;
                    if(typeof stickyCrumb.settings.toTopIcon !== 'undefined') stickyCrumb.config.totop.icon = stickyCrumb.settings.toTopIcon;
                    if(typeof stickyCrumb.settings.toTopSize === 'number') stickyCrumb.config.totop.size = stickyCrumb.settings.toTopSize;
            }


            if(!document.querySelector(stickyCrumb.config.containerSelector) || typeof stickyCrumb.containerElement.outerHTML === 'undefined') {
                stickyCrumb.config.breadcrumb = 0;
            }

            //if(document.querySelector(stickyCrumb.config.containerSelector)) {
                //stickyCrumb.containerElement = document.querySelector(stickyCrumb.config.containerSelector);
                //stickyCrumb.listElement = document.querySelector(stickyCrumb.config.containerSelector+' '+stickyCrumb.config.listSelector);
            //}

            if(!!stickyCrumb.touchOnly && !this.isTouchSupported) return;

        stickyCrumb.style.create();

            if(!!stickyCrumb.config.breadcrumb) stickyCrumb.setup();


        window.onscroll = function() {

                if(!!stickyCrumb.config.breadcrumb) stickyCrumb.setup();

                if(!!stickyCrumb.config.totop.button && window.pageYOffset > window.innerHeight) {

                        if(!document.getElementById('scBtnTop')) stickyCrumb.buttonTop.create();

                }else{

                        if(document.getElementById('scBtnTop')) stickyCrumb.buttonTop.remove();

                }
        };


        var resizeTimer;

        window.onresize = function() {

                if(resizeTimer)	clearTimeout(resizeTimer);

            resizeTimer = setTimeout(function() {

                    if(!!stickyCrumb.config.breadcrumb) stickyCrumb.setup();

            }, 250);

        };
    },

    setup: function(){
            //if(window.innerWidth >= stickyCrumb.config.breakpoint) (!stickyCrumb.isInViewport(stickyCrumb.containerElement) ? stickyCrumb.setSticky() : stickyCrumb.unsetSticky() );
            if(window.innerWidth >= stickyCrumb.config.breakpoint) (stickyCrumb.isInViewport(stickyCrumb.containerElement) ? stickyCrumb.setSticky() : stickyCrumb.unsetSticky() );
            else (!stickyCrumb.isNotInViewport(stickyCrumb.containerElement) ? stickyCrumb.setSticky() : stickyCrumb.unsetSticky() );
    },

    setSticky: function(){

            if(!stickyCrumb.isSticky() && document.documentElement.clientWidth <= stickyCrumb.config.breakpoint){
                stickyCrumb.stickyElement = stickyCrumb.containerElement.cloneNode(true);
                stickyCrumb.stickyElement.id = 'scStickyElement';
                stickyCrumb.stickyElement.style.position = 'fixed';
                stickyCrumb.stickyElement.style.left = 0;
                stickyCrumb.stickyElement.style.right = 0;
                stickyCrumb.stickyElement.style.height = 'auto';
                stickyCrumb.stickyElement.style.top = 0;
                document.body.appendChild(stickyCrumb.stickyElement);
            }
    },

    unsetSticky: function(){

            if(stickyCrumb.isSticky()){
                    if(stickyCrumb.stickyElement) {
                        stickyCrumb.stickyElement.parentNode.removeChild(stickyCrumb.stickyElement);
                        stickyCrumb.stickyElement = null;
                    }
            }
    },

    isSticky: function(){

        return (stickyCrumb.stickyElement ? true : false);
    },

    isNotInViewport: function (elem) {

        var distance = elem.getBoundingClientRect(),
            hitpoint = distance.top + (Math.abs(distance.top), elem.clientHeight);

        return (
            hitpoint >= 0 &&
            distance.left >= 0 &&
            distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            distance.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    },
    /*!
     * Determine if an element is in the viewport
     * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
     * @param  {Node}    elem The element
     * @return {Boolean}      Returns true if element is in the viewport
     */
    isInViewport: function (elem) {

        var distance = elem.getBoundingClientRect();

        return (
            distance.top >= 0 &&
            distance.left >= 0 &&
            distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            distance.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    },

    style: {

        create: function() {

            var sheet = (function () {

                var style = document.createElement("style");
                var styleStr = '@media(max-width: ' + stickyCrumb.config.breakpoint + 'px){';

                    //breadcrumb
                    if(!!stickyCrumb.config.breadcrumb) {

                        var curBgColor = window.getComputedStyle(stickyCrumb.listElement).getPropertyValue('background-color');
                        var rgb = curBgColor.replace(/^(rgb|rgba)\(/, '').replace(/\)$/, '').replace(/\s/g, '').split(',');
                        var blockWidth = stickyCrumb.listElement.children[stickyCrumb.listElement.children.length - 1].clientWidth/2;
                        styleStr += '#scStickyElement{max-width: 100%;min-width: 100%;}';
                        styleStr += '#scStickyElement:after{content: "";position: absolute;width: ' + Math.round(blockWidth / 2) + 'px;height: 100%;right: 0;top: 0;bottom: 0;';

                            if (stickyCrumb.isTouchSupported) styleStr += 'background:' + curBgColor + ';background:linear-gradient(90deg, rgba(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ', 0.2) 0%, rgba(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ', 1) 100%);';

                        styleStr += '}';

                            if (stickyCrumb.isTouchSupported) {
                                styleStr += '#scStickyElement ' + stickyCrumb.config.listSelector + '{';
                                styleStr += '-webkit-overflow-scrolling: touch;display: flex;flex-wrap: nowrap;overflow-x:auto;overflow-y:hidden;width:100%;min-width:100%;';
                                styleStr += 'display:flex;flex-wrap:nowrap;overflow-x:auto;overflow-y:hidden;width:100%;min-width:100%;';

                                    /*if( stickyCrumb.containerElement.clientWidth < (stickyCrumb.listElement.children.length*blockWidth)+blockWidth / 2 ) {
                                        styleStr += 'cursor: grab;';
                                        //maybe use this to scroll
                                    }*/

                                styleStr += '}';
                                styleStr += '#scStickyElement ' + stickyCrumb.config.listSelector + ' > li{flex:0 0 auto;white-space:nowrap}';
                                styleStr += '#scStickyElement ' + stickyCrumb.config.listSelector + ' > li:last-child{position:relative}';

                            }
                        styleStr += '#scStickyElement ' + stickyCrumb.config.listSelector + ' > li{flex: 0 0 auto; white-space: nowrap;}';
                        styleStr += '#scStickyElement ' + stickyCrumb.config.listSelector + ' > li:last-child:after{content:"";position:absolute;width:' + blockWidth + 'px;height:100%;left:100%;top:0;bottom:0;}';

                        curBgColor = rgb = blockWidth = null;
                    }
                    // /breadcrumb

                styleStr += '}';//media

                    //topButton
                    if(!!stickyCrumb.config.totop.button) {
                        //styleStr += '#scBtnTop{box-sizing:border-box;}';
                        styleStr += '#scBtnTop{box-sizing:border-box;position:fixed;bottom:10px;right:10px;width:'+stickyCrumb.config.totop.size+'px;height:'+stickyCrumb.config.totop.size+'px;border-radius:'+(stickyCrumb.config.totop.size/2)+'px;padding:'+Math.floor(stickyCrumb.config.totop.size/3)+'px;background-color:#000;text-decoration:none;}';
                        styleStr += '#scBtnTop > img{width:100%;height:auto;}';
                    }
                    // /topButton

                style.appendChild(document.createTextNode(styleStr));
                document.head.insertBefore(style, document.head.children[0]);
                styleStr = null;
                return style.sheet;

            })();

            sheet = null;
        }
    },

    buttonTop: {

        create: function(){
            var scBtn = document.createElement('a');
            scBtn.id = 'scBtnTop';
            scBtn.href = '#top';
            scBtn.title = 'Go to top';
            scBtn.onclick = stickyCrumb.buttonTop.go;
            scBtn.appendChild(stickyCrumb.buttonTop.icon.create());
            document.body.appendChild(scBtn);
        },

        remove: function(){

            document.getElementById('scBtnTop').remove();

        },

        go: function(evt){
            evt.preventDefault();
            window.scrollTo(0,0);
        },
        
        icon: {

            create: function(){

                var svgEncoded = window.btoa(unescape(encodeURIComponent(stickyCrumb.config.totop.icon)));
                var image = new Image();
                //image.width = 128;
                //image.height = 128;
                image.src = 'data:image/svg+xml;base64,'+svgEncoded;
                //image.onload = function() {};
                return image;
            }

        }
    }

};